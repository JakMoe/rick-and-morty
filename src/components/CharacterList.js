import React, { Component } from 'react'
import './CharacterList.css'
import { Link } from "react-router-dom"

export class CharacterList extends Component {
    state = {
        character: [],
        isLoading: true,
        url: 'https://rickandmortyapi.com/api/character/',
        detailsUrl: '',
        count:0,
        viewList:[],
        isError:false
    }

    fetchChar() {
        fetch('https://rickandmortyapi.com/api/character/')
            .then(res => res.json())
            .then(data => {
                this.setState({count: data.info.count});
                this.fetchEmAll();}
            ).catch(error=> {this.setState({isError: true})})
            }
    fetchEmAll(){
        let url = 'https://rickandmortyapi.com/api/character/1';
        for (let i =2; i <= this.state.count; i++){
            url = url.concat(',' + i);
        }
            fetch(url)
            .then(res => res.json())
            .then(res => {
                this.setState({character: res, viewList: res, isLoading:false});
            }).catch(error=> {this.setState({isError: true})})
    }
    handleChange =(event) =>{
        let tempArray= [];
        this.state.character.map(element =>{
            if(element.name.toUpperCase().includes(event.target.value.toUpperCase())){
                tempArray.push(element);
            }
        });
        this.setState({viewList:tempArray});
    }


    render() {
        const {viewList,isLoading,url, isError} = this.state;
        return (
            <React.Fragment>
                <div className="wrapper">
                    <input type="text" className="searcher" placeholder="enter a characters name.." onChange={this.handleChange}/>
                </div>
                <div className="row">
                    {
                        !isLoading ? (
                            viewList.map(char => {
                                const image = char.image;
                                const id = char.id;
                                const name = char.name;
                                const status = char.status;
                                const species = char.species;
                                const gender = char.gender;
                                const origin = char.origin.name;
                                const lastLocation = char.location.name;
                                //console.log(image);
                                return (
                                    <Link to={{
                                        pathname:`details/${id}`,
                                        state:{
                                            id:id

                                        }
                                    }}>
                                        <div className="groupCard" key={id} onClick={(e) => { this.setState({ detailsUrl: url.concat(id) }); console.log(this.state.detailsUrl); }
                                        } >
                                            <div className="image-container" style={{ backgroundImage: 'url(' + image + ')' }}>
                                                <div className="image-text">{name}</div>
                                            </div>
                                            <div className="container">
                                                <table className="listTable">
                                                    <tbody>
                                                        <tr><td><b>Status: </b></td>
                                                            <td className="listRight">{status}</td>
                                                        </tr>
                                                        <tr><td><b>Species: </b></td>
                                                            <td className="listRight">{species}</td>
                                                        </tr>
                                                        <tr><td><b>Gender: </b></td>
                                                            <td className="listRight">{gender}</td>
                                                        </tr>
                                                        <tr><td><b>Origin: </b></td>
                                                            <td className="listRight"> {origin}</td>
                                                        </tr>
                                                        <tr><td><b>Last Location: </b></td>
                                                            <td className="listRight">{lastLocation} </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </Link>
                                )

                            }
                            )) : (<div></div>)
                    } 
                     {isError ? (
                         <div className="error">
                        <h1>Run out of requests, try again later</h1></div>
                    ): (
                        <div></div>
                    )}
                </div>



            </React.Fragment>

        )
    }

    componentDidMount() {
        this.fetchChar();
    }

}







export default CharacterList;
