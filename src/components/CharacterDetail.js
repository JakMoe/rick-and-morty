import React, { Component } from 'react'
import './CharacterDetail.css'

export class CharacterDetail extends Component {
    state = {
        character: [],
        location: [],
        isLoading: true,
    }
    plsGiveFeedback() {
        if (this.props.match.params.id) {
            const paramId = this.props.match.params.id;
            fetch(`https://rickandmortyapi.com/api/character/${paramId}`)
                .then(res => res.json())
                .then(res => {
                    this.setState({
                        character: res
                    }); if (res.location.name != 'unknown') {this.fetchLocation(res.location.url);} else {
                    this.setState({isLoading: false, location: [{name: 'unknown', type: 'unknown', dimension: 'unknown'}]});}
                })
        }
    }
    fetchLocation(url) {
        fetch(url)
            .then(res => res.json())
            .then(res => {
                this.setState({
                    location: res, isLoading: false

                });
            })
    }
    render() {
        const { character, isLoading, location, isError } = this.state;

        return (
            <React.Fragment>

                {!isLoading ? (
                    <div className="container">
                        <table className="table" border="0">
                            <tbody className="body">
                                <tr>
                                    <td className="Char-img">
                                        <img src={character.image} style={{ maxWidth: '170px', display: 'block' }} />
                                        <div className="titleName"> <h4>{character.name}</h4>
                                            <span>Status: {character.status}</span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <h3>Profile</h3>
                                </tr>
                                <tr>
                                    <td>
                                        Gender: 
                                    </td>
                                    <td align="right">{character.gender}</td>
                                </tr>
                                <tr>
                                    <td>
                                        Species: 
                                    </td>
                                    <td align="right">{character.species}</td>
                                </tr>
                                <tr>
                                    <td>
                                        Origin: 
                                    </td>
                                    <td align="right">{character.origin.name}</td>
                                </tr>
                                <tr>
                                    <h3>Location</h3>
                                </tr>
                                <tr>
                                    <td>
                                        Current location: 
                                    </td>
                                    <td align="right">{location.name}</td>
                                </tr>
                                <tr>
                                    <td>
                                        Type: 
                                    </td>
                                    <td align="right">{location.type}</td>
                                </tr>
                                <tr>
                                    <td>
                                        Dimension: 
                                    </td>
                                    <td align ="right">{location.dimension}
                                    <br/>
                                    <br/>
                                <button onClick={(e)=> this.props.history.push('/')}>BACK</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                ) : (
                        <div>O.o</div>
                    )}





            </React.Fragment>
        )
    }
    componentDidMount() {
        this.plsGiveFeedback()
    }
}

export default CharacterDetail
