
import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { CharacterList } from './components/CharacterList';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { CharacterDetail } from './components/CharacterDetail';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <header></header>
          <Switch>
            <Route exact path="/">
              <CharacterList />
            </Route>
            <Route exact path="/details/:id" component={CharacterDetail}> 
            </Route>
          </Switch>
        </div>
      </Router>

    )
  };
}


export default App;
